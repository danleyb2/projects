import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 *   write and test a program
     that converts words to pig Latin. Words that begin with a vowel are unchanged.
     Words that begin with a consonant have the starting consonant moved to the end of
     the word and “ay” is appended after the consonant. If the input is only the single
     letter “q” or “Q,” the program quits. Given the inputs “open,” “door,” and “q,” make
     the program produce the following display.

     Sample session:
     ```
     Pig Latin Translator
     Enter a word: open
     In Pig Latin, that's "open"
     Enter a word: door
     In Pig Latin, that's "oorday"
     Enter a word: q
     I hope you enjoyed your translation experience! Please come again.
     ```

 *
 *
 */

public class PigLatin {

    private static boolean isVowel(char c){
        return "aeiou".indexOf(c) > 0;
    }

    public static void main(String[] args){

        System.out.println("Pig Latin Translator");

        String TERMINATION_INPUT = "q";
        boolean okInput=true;

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        // keep getting user input
        System.out.print("\nEnter a word:.");
        String inputWord;
        try {
            inputWord = in.readLine();
            while (!inputWord.equals(TERMINATION_INPUT)) {

                for (int i = 0; i < inputWord.length(); i++) {
                    if (!Character.isLetter(inputWord.charAt(i))){
                        okInput=false;
                        System.out.println("Invalid input ─ can’t process this character:  "+inputWord.charAt(i));
                        break;
                    }
                }
                if(okInput){
                    String pigWord = "";
                    String suffix;
                    char firstChar = inputWord.charAt(0);
                    if(isVowel(Character.toLowerCase(firstChar))){
                        pigWord+=firstChar;
                        suffix = "";
                    }else {
                        suffix = firstChar+"ay";
                    }
                    pigWord+=inputWord.substring(1);
                    pigWord+=suffix;

                    System.out.println("in Pig Latin, that’s "+ pigWord);

                }

                System.out.print("\nEnter a word:.");
                inputWord = in.readLine();
            }
            System.out.println("I hope you enjoyed your translation experience! Please come again.");

        }catch (IOException e){
            e.printStackTrace();
        }

    }


}
